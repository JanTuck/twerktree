package me.jantuck.twerktree.event;

import me.jantuck.twerktree.reflection.MinecraftReflectionProvider;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

/**
 * Created by Jan on 1/22/2017.
 */
public class PlayerEvents implements Listener {

    @EventHandler
    public void onPlayerSneakEvent(org.bukkit.event.player.PlayerToggleSneakEvent event) {
        Player player = event.getPlayer();
        if (!player.isSneaking())
            return;
        World world = player.getWorld();
        for (int y = (int) player.getLocation().getY(); y >= 0; --y) {
            for (int xOff = -5; xOff <= 5; ++xOff) {
                for (int zOff = -5; zOff <= 5; ++zOff) {
                    Block block = world.getBlockAt((int) player.getLocation().getX() + xOff, y, (int) player.getLocation().getZ() + zOff);
                    if (block.getType() == Material.SAPLING) {
                        MinecraftReflectionProvider.boneMeal(block.getLocation());
                    }
                }
            }
        }
    }
}
