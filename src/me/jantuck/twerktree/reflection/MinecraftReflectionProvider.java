package me.jantuck.twerktree.reflection;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;


/**
 * Created by Jan on 18-02-2017.
 */
public class MinecraftReflectionProvider {
    public static final String MC_VERSION = Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];
    public static final Class<?> CRAFT_ITEM_STACK = getCBClass("inventory.CraftItemStack");
    public static final ItemStack BONE_MEAL = new ItemStack(Material.INK_SACK, 1, (byte) 15);
    public static final Class<?> CRAFT_WORLD = getCBClass("CraftWorld");
    public static final Class<?> NMS_ITEM_STACK = getNMSClass("ItemStack");
    public static final Class<?> ITEM_DYE = getNMSClass("ItemDye");
    public static final Class<?> NMS_WORLD = getNMSClass("World");
    public static final Class<?> NMS_BLOCK_POSITION = getNMSClass("BlockPosition");

    public static Object NMSItemStack = ReflectionUtil.newCall().getMethod(CRAFT_ITEM_STACK, "asNMSCopy", ItemStack.class)
            .get().invokeIfValid(BONE_MEAL, BONE_MEAL);

    public static Class<?> getCBClass(String name) {
        try {
            return Class.forName("org.bukkit.craftbukkit." + MC_VERSION + "." + name);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Class<?> getNMSClass(String name) {
        try {
            return Class.forName("net.minecraft.server." + MC_VERSION + "." + name);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static void boneMeal(Location location) {
        ReflectionUtil.newCall().getMethod(CRAFT_WORLD, "getHandle")
                .get()
                .passIfValid(reflectionMethod -> {
                    Object world = reflectionMethod.invokeIfValid(location.getWorld());
                    ReflectionUtil.newCall().getMethod(ITEM_DYE, "a", NMS_ITEM_STACK, NMS_WORLD, NMS_BLOCK_POSITION)
                            .get()
                            .passIfValid(reflectionMethod1 -> {
                                ReflectionUtil.newCall().getConstructor(NMS_BLOCK_POSITION, int.class, int.class, int.class)
                                        .get()
                                        .passIfValid(reflectionConstructor -> {
                                            Object blockPosition = reflectionConstructor.newInstance((int) location.getX(), (int) location.getY(), (int) location.getZ());
                                            reflectionMethod1.invokeIfValid(null, NMSItemStack, world, blockPosition);
                                        });
                            });
                });
    }
}
