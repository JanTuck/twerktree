package me.jantuck.twerktree;

import me.jantuck.twerktree.event.PlayerEvents;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by Jan on 1/22/2017.
 */
public class Core extends JavaPlugin {
    private static Core instance = null;

    public static Core getInstance() {
        return instance;
    }

    @Override
    public void onEnable() {
        instance = this;
        getServer().getPluginManager().registerEvents(new PlayerEvents(), this);
    }
}
